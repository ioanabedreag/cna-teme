﻿using Grpc.Core;
using System;
using System.Threading;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            do
            {
                Console.WriteLine("Please enter your name: ");

                var name = Console.ReadLine();

                var client = new Generated.NameOperationService.NameOperationServiceClient(channel);

                try
                {
                    var response = client.SayHello(new Generated.NameRequest
                    {
                        Name = name
                    });
                    Thread.Sleep(100);
                }

                catch (Exception e)
                {
                }

            } while (true);

            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
