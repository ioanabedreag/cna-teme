﻿using Generated;
using Grpc.Core;
using System.Threading.Tasks;

namespace Server
{
    internal class NameOperationService : Generated.NameOperationService.NameOperationServiceBase

    {
        public override Task<OperationResponse> SayHello(NameRequest request, ServerCallContext context)
        {
            var message = request.Name;

            System.Console.Write("Name: ");
            System.Console.WriteLine(message);

            return Task.FromResult(new OperationResponse() { //Message = message 
            });
        }
    }
}
