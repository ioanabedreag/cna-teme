﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrpcServer
{
    class Configuration
    {
        public const string HOST = "localhost";
        public const int PORT = 16842;
    }
}
